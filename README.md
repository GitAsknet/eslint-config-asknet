# Asknet Frontend ESLint Rules for JavaScript and TypeScript

# Index

* [Common](#common)
* [JavaScript](#javascript)
* [TypeScript](#typescript)
* [Git Hooks](#git-hooks)

## Common

1. Install the asknet eslint config.

	```shellscript
	npm install --save-dev git+ssh://git@bitbucket.org/GitAsknet/eslint-config-asknet.git
	```

1. Install all peer dependencies if necessary.

	```shellscript
	npm install --save-dev @typescript-eslint/eslint-plugin @typescript-eslint/eslint-plugin-tslint @typescript-eslint/parser eslint eslint-plugin-import eslint-plugin-jsdoc
	```

1. Create the file `.eslintignore` and add files and folders which should be ignored.


## JavaScript

4. Create the file `.eslintrc.json` and add the following lines for javascript linting.

	```json
	{
		"extends": [
			"asknet/javascript"
		]
	}
	```


## TypeScript

4. Create the file `.eslintrc.json` and add the following lines for typescript linting.

	```json
	{
		"extends": [
			"asknet/typescript"
		]
	}
	```


## Git Hooks

Requires git version 2.9 or higher.

1. Create a folder in the root of the repository.

	```shellscript
	mkdir .githooks
	```

1. Copy the file `pre-commit` into the new folder `.githooks`.

	```shellscript
	cp ./node_modules/eslint-config-asknet/.githooks/pre-commit ./.githooks
	```

1. Set the hooks path for the git config.

	```shellscript
	git config --local core.hooksPath .githooks
	# or global
	git config core.hooksPath .githooks
	```

1. Install gulp >= 4.0.2

	```shellscript
	npm install --save-dev gulp
	```

1. Create the `gulpfile.js` for the lint task.

	```js
	const gulp = require('gulp');
	const { ESLint } = require('eslint');
	
	async function lint (paths) {
		const eslint = new ESLint();
		const results = await eslint.lintFiles(paths);
		const formatter = await eslint.loadFormatter('stylish');
		const resultText = formatter.format(results);
		if (resultText) {
			console.log(resultText);
			if (ESLint.getErrorResults(results).length) {
				process.exit(1);
			}
		}
	}
	gulp.task('lint', () => {
		return lint([
			'src/**/*.js',
			// 'src/**/*.ts',
		]);
	});
	```

1. Add the lint task and the local githooks path to `package.json`.

	```json
	{
		"scripts": {
			"lint": "gulp lint"
		}
	}
	```

