module.exports = {
	env: {
		es6: true,
		node: true
	},
	extends: [
		"eslint:recommended",
		"plugin:@typescript-eslint/recommended",
		"plugin:@typescript-eslint/recommended-requiring-type-checking"
	],
	parser: "@typescript-eslint/parser",
	parserOptions: {
		"project": "tsconfig.json",
		"sourceType": "module"
	},
	plugins: [
		"eslint-plugin-import",
		"eslint-plugin-indent-empty-lines",
		"eslint-plugin-jsdoc",
		"@typescript-eslint",
		"@typescript-eslint/tslint"
	],
	rules: {
		"@typescript-eslint/array-type": [
			"error",
			{
				default: "array-simple"
			}
		],
		"@typescript-eslint/ban-types": [
			"error",
			{
				types: {
					Object: {
						message: "Avoid using the `Object` type. Did you mean `object`?"
					},
					Function: {
						message: "Avoid using the `Function` type. Prefer a specific function type, like `() => void`."
					},
					Boolean: {
						message: "Avoid using the `Boolean` type. Did you mean `boolean`?"
					},
					Number: {
						message: "Avoid using the `Number` type. Did you mean `number`?"
					},
					String: {
						message: "Avoid using the `String` type. Did you mean `string`?"
					},
					Symbol: {
						message: "Avoid using the `Symbol` type. Did you mean `symbol`?"
					}
				}
			}
		],
		"@typescript-eslint/consistent-type-assertions": [
			"error",
			{
				assertionStyle: "angle-bracket"
			}
		],
		"@typescript-eslint/consistent-type-definitions": "off",
		"@typescript-eslint/explicit-module-boundary-types": "off",
		"@typescript-eslint/member-ordering": "off",
		"@typescript-eslint/no-dupe-class-members": "error",
		"@typescript-eslint/no-explicit-any": "off",
		"@typescript-eslint/no-floating-promises": "off",
		"@typescript-eslint/no-misused-promises": [
			"error",
			{
				"checksVoidReturn": false
			}
		],
		"@typescript-eslint/no-parameter-properties": "off",
		"@typescript-eslint/no-require-imports": "error",
		"@typescript-eslint/no-unsafe-assignment": "off",
		"@typescript-eslint/no-unsafe-member-access": "off",
		"@typescript-eslint/no-unsafe-return": "off",
		"@typescript-eslint/prefer-for-of": "error",
		"@typescript-eslint/prefer-function-type": "error",
		"@typescript-eslint/prefer-literal-enum-member": "error",
		"@typescript-eslint/restrict-template-expressions": "off",
		"@typescript-eslint/type-annotation-spacing": [
			"error",
			{
				before: false,
				after: true,
				overrides: {
					arrow: {
						before: true,
						after: true
					}
				}
			}
		],
		"@typescript-eslint/triple-slash-reference": [
			"error",
			{
				path: "never",
				types: "never",
				lib: "never"
			}
		],
		"@typescript-eslint/unified-signatures": "error",
		"@typescript-eslint/tslint/config": [
			"error",
			{
				rules: {
					whitespace: [
						true,
						"check-type-operator"
					]
				}
			}
		],
		"import/exports-last": "error",
		"import/first": "error",
		"import/newline-after-import": "error",
		"import/no-dynamic-require": "error",
		"import/order": "error",
		"indent-empty-lines/indent-empty-lines": [
			"error",
			"tab"
		],
		"jsdoc/check-alignment": "error",
		"jsdoc/check-indentation": "error",
		"jsdoc/newline-after-description": "error",
		"array-bracket-spacing": "error",
		"array-callback-return": "error",
		"arrow-parens": "error",
		"arrow-spacing": "error",
		"block-spacing": "error",
		"brace-style": "off",
		"camelcase": "off",
		"@typescript-eslint/naming-convention": [
			"error",
			{
				selector: "default",
				format: [
					"camelCase",
					"PascalCase",
					"UPPER_CASE"
				],
				leadingUnderscore: "allow",
				trailingUnderscore: "forbid"
			}
		],
		"@typescript-eslint/brace-style": "error",
		"comma-dangle": "off",
		"@typescript-eslint/comma-dangle": [
			"error",
			{
				arrays: "always-multiline",
				enums: "always-multiline",
				exports: "always-multiline",
				functions: "never",
				generics: "always-multiline",
				imports: "always-multiline",
				objects: "always-multiline",
				tuples: "always-multiline"
			}
		],
		"comma-spacing": "off",
		"@typescript-eslint/comma-spacing": "error",
		"comma-style": "error",
		"complexity": "off",
		"curly": [
			"error",
			"multi-line"
		],
		"default-case-last": "error",
		"default-param-last": "off",
		"@typescript-eslint/default-param-last": "error",
		"dot-location": [
			"error",
			"property"
		],
		"dot-notation": "off",
		"@typescript-eslint/dot-notation": "error",
		"eol-last": [
			"error",
			"never"
		],
		"eqeqeq": [
			"error",
			"smart"
		],
		"func-call-spacing": "off",
		"@typescript-eslint/func-call-spacing": "error",
		"func-style": [
			"error",
			"declaration"
		],
		"function-paren-newline": [
			"error",
			"never"
		],
		"grouped-accessor-pairs": [
			"error",
			"getBeforeSet"
		],
		"guard-for-in": "error",
		"id-denylist": [
			"error",
			"any",
			"bool",
			"boolean",
			"err",
			"num",
			"number",
			"obj",
			"object",
			"str",
			"string",
			"Undefined",
			"val"
		],
		"id-match": "error",
		"id-length": [
			"error",
			{
				exceptionPatterns: [
					"^[i-k]$"
				]
			}
		],
		"implicit-arrow-linebreak": "error",
		"indent": "off",
		"@typescript-eslint/indent": [
			"error",
			"tab",
			{
				ignoreComments: true,
				ArrayExpression: 1,
				FunctionDeclaration: {
					body: 1
				},
				MemberExpression: 1,
				ObjectExpression: 1,
				SwitchCase: 1
			}
		],
		"key-spacing": "error",
		"keyword-spacing": "off",
		"@typescript-eslint/keyword-spacing": "error",
		"lines-between-class-members": "off",
		"@typescript-eslint/lines-between-class-members": "error",
		"max-classes-per-file": "error",
		"max-len": [
			"warn",
			{
				code: 160,
				ignoreStrings: true,
				ignoreTemplateLiterals: true,
				ignoreTrailingComments: true
			}
		],
		"multiline-ternary": [
			"error",
			"never"
		],
		"new-parens": "error",
		"no-bitwise": "error",
		"no-caller": "error",
		"no-console": "warn",
		"no-duplicate-imports": "off",
		"@typescript-eslint/no-duplicate-imports": "error",
		"no-else-return": "error",
		"no-eval": "error",
		"no-extra-parens": "off",
		"@typescript-eslint/no-extra-parens": [
			"error",
			"all",
			{
				conditionalAssign: false
			}
		],
		"no-invalid-this": "off",
		"@typescript-eslint/no-invalid-this": "error",
		"no-label-var": "error",
		"no-lonely-if": "error",
		"no-loop-func": "off",
		"@typescript-eslint/no-loop-func": "error",
		"no-loss-of-precision": "off",
		"@typescript-eslint/no-loss-of-precision": "error",
		"no-multiple-empty-lines": [
			"error",
			{
				max: 1,
				maxBOF: 0,
				maxEOF: 0
			}
		],
		"no-multi-spaces": "error",
		"no-multi-str": "error",
		"no-new": "error",
		"no-new-wrappers": "error",
		"no-new-func": "error",
		"no-nonoctal-decimal-escape": "error",
		"no-octal-escape": "error",
		"no-param-reassign": "error",
		"no-proto": "error",
		"no-redeclare": "off",
		"@typescript-eslint/no-redeclare": "error",
		"no-restricted-syntax": [
			"error",
			"WithStatement"
		],
		"no-return-assign": "error",
		"no-script-url": "error",
		"no-self-compare": "error",
		"no-sequences": "error",
		"no-shadow": "off",
		"@typescript-eslint/no-shadow": [
			"error",
			{
				hoist: "all"
			}
		],
		"no-template-curly-in-string": "error",
		"no-throw-literal": "off",
		"@typescript-eslint/no-throw-literal": "error",
		"no-trailing-spaces": [
			"error",
			{
				skipBlankLines: true,
				ignoreComments: true
			}
		],
		"no-undef-init": "error",
		"no-underscore-dangle": "error",
		"no-unneeded-ternary": "error",
		"no-unreachable-loop": "error",
		"no-unused-expressions": "off",
		"@typescript-eslint/no-unused-expressions": "error",
		"no-use-before-define": "off",
		"@typescript-eslint/no-use-before-define": "error",
		"no-useless-concat": "error",
		"no-useless-constructor": "off",
		"@typescript-eslint/no-useless-constructor": "error",
		"no-useless-rename": "error",
		"no-whitespace-before-property": "error",
		"nonblock-statement-body-position": "error",
		"object-shorthand": "error",
		"one-var": [
			"error",
			"never"
		],
		"operator-assignment": "error",
		"operator-linebreak": [
			"error",
			"before"
		],
		"padded-blocks": [
			"error",
			{
				"classes": "always",
				"switches": "never"
			}
		],
		"padding-line-between-statements": [
			"error",
			{
				"blankLine": "always",
				"prev": "*",
				"next": [
					"class",
					"export",
					"function"
				]
			},
			{
				"blankLine": "never",
				"prev": "*",
				"next": [
					"return"
				]
			}
		],
		"prefer-regex-literals": "error",
		"prefer-template": "error",
		"object-curly-spacing": "off",
		"@typescript-eslint/object-curly-spacing": [
			"error",
			"always"
		],
		"quotes": "off",
		"@typescript-eslint/quotes": [
			"error",
			"single"
		],
		"radix": "error",
		"rest-spread-spacing": "error",
		"semi": "off",
		"@typescript-eslint/semi": "error",
		"semi-spacing": "error",
		"semi-style": "error",
		"space-before-blocks": "error",
		"space-before-function-paren": "off",
		"@typescript-eslint/space-before-function-paren": "error",
		"space-in-parens": "error",
		"space-infix-ops": "off",
		"@typescript-eslint/space-infix-ops": "error",
		"space-unary-ops": "error",
		"symbol-description": "error",
		"template-curly-spacing": "error",
		"template-tag-spacing": "error",
		"yield-star-spacing": "error",
		"yoda": [
			"error",
			"never",
			{
				exceptRange: true
			}
		]
	}
}