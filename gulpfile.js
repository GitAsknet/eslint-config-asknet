const gulp = require('gulp');
const { ESLint } = require('eslint');

async function lint (paths) {
	const eslint = new ESLint();
	const results = await eslint.lintFiles(paths);
	const formatter = await eslint.loadFormatter('stylish');
	const resultText = formatter.format(results);
	if (resultText) {
		console.log(resultText);
		if (ESLint.getErrorResults(results).length) {
			process.exit(1);
		}
	}
}

gulp.task('lint', () => {
	return lint([
		'src/**/*.js',
		// 'src/**/*.ts',
	]);
});