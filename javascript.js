module.exports = {
	env: {
		es6: true,
		node: true
	},
	parserOptions: {
		ecmaVersion: 8
	},
	extends: [
		"eslint:recommended"
	],
	plugins: [
		"eslint-plugin-import",
		"eslint-plugin-jsdoc"
	],
	rules: {
		"import/exports-last": "error",
		"import/first": "error",
		"import/newline-after-import": "error",
		"import/no-dynamic-require": "error",
		"import/order": "error",
		"indent-empty-lines/indent-empty-lines": [
			"error",
			"tab"
		],
		"jsdoc/check-alignment": "error",
		"jsdoc/check-indentation": "error",
		"jsdoc/newline-after-description": "error",
		"array-bracket-spacing": "error",
		"array-callback-return": "error",
		"arrow-parens": "error",
		"arrow-spacing": "error",
		"block-spacing": "error",
		"brace-style": "error",
		"camelcase": "error",
		"comma-dangle": [
			"error",
			{
				arrays: "always-multiline",
				exports: "always-multiline",
				functions: "never",
				imports: "always-multiline",
				objects: "always-multiline"
			}
		],
		"comma-spacing": "error",
		"comma-style": "error",
		"complexity": "off",
		"curly": [
			"error",
			"multi-line"
		],
		"default-case-last": "error",
		"default-param-last": "error",
		"dot-location": [
			"error",
			"property"
		],
		"dot-notation": "error",
		"eol-last": [
			"error",
			"never"
		],
		"eqeqeq": [
			"error",
			"smart"
		],
		"func-call-spacing": "error",
		"func-style": [
			"error",
			"declaration"
		],
		"function-paren-newline": [
			"error",
			"never"
		],
		"grouped-accessor-pairs": [
			"error",
			"getBeforeSet"
		],
		"guard-for-in": "error",
		"id-denylist": [
			"error",
			"any",
			"bool",
			"boolean",
			"err",
			"num",
			"number",
			"obj",
			"object",
			"str",
			"string",
			"Undefined",
			"val"
		],
		"id-match": "error",
		"id-length": [
			"error",
			{
				exceptionPatterns: [
					"^[i-k]$"
				]
			}
		],
		"implicit-arrow-linebreak": "error",
		"indent": [
			"error",
			"tab",
			{
				ignoreComments: true,
				ArrayExpression: 1,
				FunctionDeclaration: {
					body: 1
				},
				MemberExpression: 1,
				ObjectExpression: 1,
				SwitchCase: 1
			}
		],
		"key-spacing": "error",
		"keyword-spacing": "error",
		"lines-between-class-members": "error",
		"max-classes-per-file": "error",
		"max-len": [
			"warn",
			{
				code: 160
			}
		],
		"multiline-ternary": [
			"error",
			"never"
		],
		"new-parens": "error",
		"no-bitwise": "error",
		"no-caller": "error",
		"no-console": "warn",
		"no-duplicate-imports": "error",
		"no-else-return": "error",
		"no-eval": "error",
		"no-extra-parens": [
			"error",
			"all",
			{
				conditionalAssign: false
			}
		],
		"no-invalid-this": "error",
		"no-label-var": "error",
		"no-lonely-if": "error",
		"no-loop-func": "error",
		"no-loss-of-precision": "error",
		"no-multiple-empty-lines": [
			"error",
			{
				max: 1,
				maxBOF: 0,
				maxEOF: 0
			}
		],
		"no-multi-spaces": "error",
		"no-multi-str": "error",
		"no-new": "error",
		"no-new-wrappers": "error",
		"no-new-func": "error",
		"no-nonoctal-decimal-escape": "error",
		"no-octal-escape": "error",
		"no-param-reassign": "error",
		"no-proto": "error",
		"no-redeclare": "error",
		"no-restricted-syntax": [
			"error",
			"WithStatement"
		],
		"no-return-assign": "error",
		"no-script-url": "error",
		"no-self-compare": "error",
		"no-sequences": "error",
		"no-shadow": [
			"error",
			{
				hoist: "all"
			}
		],
		"no-template-curly-in-string": "error",
		"no-throw-literal": "error",
		"no-trailing-spaces": [
			"error",
			{
				skipBlankLines: true,
				ignoreComments: true
			}
		],
		"no-undef-init": "error",
		"no-underscore-dangle": "off",
		"no-unneeded-ternary": "error",
		"no-unreachable-loop": "error",
		"no-unsafe-return": "off",
		"no-unused-expressions": "error",
		"no-use-before-define": "error",
		"no-useless-concat": "error",
		"no-useless-constructor": "error",
		"no-useless-rename": "error",
		"no-whitespace-before-property": "error",
		"nonblock-statement-body-position": "error",
		"object-shorthand": "error",
		"one-var": [
			"error",
			"never"
		],
		"operator-assignment": "error",
		"operator-linebreak": [
			"error",
			"before"
		],
		"padded-blocks": [
			"error",
			{
				classes: "always",
				switches: "never"
			}
		],
		"prefer-regex-literals": "error",
		"prefer-template": "error",
		"object-curly-spacing": [
			"error",
			"always"
		],
		"quotes": [
			"error",
			"single"
		],
		"radix": "error",
		"rest-spread-spacing": "error",
		"semi": "error",
		"semi-spacing": "error",
		"semi-style": "error",
		"space-before-blocks": "error",
		"space-before-function-paren": "error",
		"space-in-parens": "error",
		"space-infix-ops": "error",
		"space-unary-ops": "error",
		"symbol-description": "error",
		"template-curly-spacing": "error",
		"template-tag-spacing": "error",
		"yield-star-spacing": "error",
		"yoda": [
			"error",
			"never",
			{
				exceptRange: true
			}
		]
	}
}